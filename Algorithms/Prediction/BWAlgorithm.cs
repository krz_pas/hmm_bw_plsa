﻿using HMMDishonestCasino.Algorithms.Probability;
using HMMDishonestCasino.Casino;
using HMMDishonestCasino.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMMDishonestCasino.Algorithms.Prediction
{
    class BWAlgorithm<TObservation, TState> : PredictionAlgorithm<TObservation, TState>
    {
      /*  private double alpha[][];
	    private double beta[][];
	    private double gamma[][];
	
    	private double xi[][][];
	
	    private double scale[];
    */
        private Dictionary<TState, double[]> alphas = new Dictionary<TState, double[]>();
        private Dictionary<TState, double[]> betas = new Dictionary<TState, double[]>();
        private Dictionary<TState, double[]> gammas = new Dictionary<TState, double[]>();
        private Dictionary<TState, double> initialStateProbabilities = new Dictionary<TState, double>();

        public MatrixHashTable<KeyValuePair<TState, TState>, int, double> newParameters;// = new MatrixHashTable<KeyValuePair<TState, TState>, TObservation, double>();
      //  private MatrixHashTable<TState, TObservation, double> newEmissionMatrix = new MatrixHashTable<TState, TObservation, double>();
        private double []sequenceProbabilities;
        int MAX_ITERATIONS = 100;
         public BWAlgorithm()
        {
        }
        
   public BWAlgorithm(BaseAlgorithm<TObservation, TState> baseAlgorithm)
       : base(baseAlgorithm)
        {
        }

#if !_OLD_VERSION

        private void initNewMatrices()
        {
            NewEmissionMatrix = new MatrixHashTable<TState, TObservation, double>(StateSpace, ObservationSpace);
            NewTransitionMatrix = new MatrixHashTable<TState, TState, double>(StateSpace, StateSpace);
            Random randomGenerator = new Random();
            foreach (var st1 in StateSpace)
            {
                double normalizingFactor = 0.0;
                foreach (var st2 in StateSpace)
                {
                    NewTransitionMatrix[st1, st2] = 1.0 / StateSpace.Length + 0.1 * randomGenerator.NextDouble();
                    normalizingFactor += NewTransitionMatrix[st1, st2];

                }
                foreach (var st2 in StateSpace)
                    NewTransitionMatrix[st1, st2] = NewTransitionMatrix[st1, st2] / normalizingFactor;
            }

            foreach (var st1 in StateSpace)
            {
                double normalizingFactor = 0.0;
                foreach (var obs1 in ObservationSpace)
                {
                    NewEmissionMatrix[st1, obs1] = 1.0 / ObservationSpace.Length + 0.1 * randomGenerator.NextDouble();
                    normalizingFactor += NewEmissionMatrix[st1, obs1];

                }
                foreach (var obs1 in ObservationSpace)
                    NewEmissionMatrix[st1, obs1] = NewEmissionMatrix[st1, obs1] / normalizingFactor;
            }
        }
        
        //the problem is, we do not work on original emission and transmission matrices...
        private void initDictionaries()
        {
            initNewMatrices();
            foreach (var state in StateSpace)
            {
                alphas.Add(state, new double[SequenceOfObservations.Length]);
                betas.Add(state, new double[SequenceOfObservations.Length]);
                gammas.Add(state, new double[SequenceOfObservations.Length]);
                initialStateProbabilities.Add(state, 1.0/StateSpace.Length);
            }
            foreach (var state in StateSpace)
            {
                for (var i =0; i <SequenceOfObservations.Length;i++)
                {
                    alphas[state][i] = 0;
                    betas[state][i] = 0;
                    gammas[state][i] = 0;
                }

            }
            List<KeyValuePair<TState, TState>> list = new List<KeyValuePair<TState, TState>>();
            foreach (var state in StateSpace)
            {
                foreach (var state2 in StateSpace)
                {
                    foreach (var obs in ObservationSpace)
                    {
                        KeyValuePair<TState, TState> kvp = new KeyValuePair<TState, TState>(state, state2);
                        list.Add(kvp);
                       // newParameters =  new MatrixHashTable<KeyValuePair<TState, TState>, TObservation, double>(new KeyValuePair<TState,TState>(StateSpace,StateSpace),ObservationSpace,0);
                    }
                }
            }
            List<int> indexes = new List<int>();

            for (var i = 0; i < SequenceOfObservations.Length; i++)
                indexes.Add(i);
           // IReadOnlyCollection<int> indexesReadOnly = new IReadOnlyCollection<int>(indexes);
           newParameters = new MatrixHashTable<KeyValuePair<TState, TState>, int, double>(list.ToArray(), indexes,0.0);
      //  private MatrixHashTable<TState, TObservation, double> newEmissionMatrix =[ new MatrixHashTable<TState, TObservation, double>();
        }
        private double  forward()
   {
       double result = 0;
       //first, we reset sequence probabilities
       for (var i = 1; i < sequenceProbabilities.Length;i++ )
       {
           sequenceProbabilities[i] = 0;
       }
//
            // we calculate seq probabilities in iterative way
       sequenceProbabilities[0] = 1;
       foreach (var state in StateSpace) //we assume initial probabilities to be equal
       {
           alphas[state][0] = initialStateProbabilities[state];
       }
       for (var i = 0; i < SequenceOfObservations.Length-1; i++ )
       {
           foreach (var nextState in StateSpace)
           {
               var sum = 0.0;
               foreach (var prevState in StateSpace)
               {
                   sum += alphas[prevState][i] * NewTransitionMatrix[prevState, nextState];
               }
               alphas[nextState][i + 1] = sum * NewEmissionMatrix[nextState, SequenceOfObservations[i + 1]];
               sequenceProbabilities[i + 1] += alphas[nextState][i + 1];
           }
           foreach (var state in StateSpace)
           {
               alphas[state][i + 1] /= sequenceProbabilities[i + 1];
           }
           
       }
           foreach (var state in StateSpace)
           {
               result += alphas[state][SequenceOfObservations.Length - 1];
           }
           return result;
   }
        private double backward()
        {
            double result = 0;
            foreach (var state in StateSpace)
            {
               betas[state][SequenceOfObservations.Length - 1] = 1.0;// / sequenceProbabilities[SequenceOfObservations.Length - 1];
            }
            for (var i = SequenceOfObservations.Length - 2; i >= 0; i-- )
            {
                foreach (var state in StateSpace)
                {
                    var sum = 0.0;
                    foreach (var state2 in StateSpace)
                    {
                        sum += NewTransitionMatrix[state, state2] * NewEmissionMatrix[state2, SequenceOfObservations[i+1]]*betas[state2][i+1];

                    }
                    betas[state][i] = sum;// / sequenceProbabilities[i];
                }
                var normalizingFactor = 0.0;
                foreach (var state in StateSpace)
                {
                    normalizingFactor += betas[state][i];
                }
                foreach (var state in StateSpace)
                {
                    betas[state][i] /= normalizingFactor;
                }
            }
            foreach (var state in StateSpace)
            {
                result += NewTransitionMatrix[StateSpace[0], state] * NewEmissionMatrix[state, SequenceOfObservations[0]]*betas[state][0];
            }
                return result;
        }
        private void gamma()
        {
            for (var i = 0; i < SequenceOfObservations.Length; i++)
            {
                var denominator = 0.0;
                foreach (var state in StateSpace)
                {
                    gammas[state][i] = alphas[state][i] * betas[state][i];
                    denominator += gammas[state][i];
                } 
                foreach (var state in StateSpace)
                {
                    gammas[state][i] /= denominator;
                }
            }   
        }
        private void recomputeMatrices()
        {
            for (var i = 0; i < SequenceOfObservations.Length-1; i++)
            {
                var sum = 0.0;
                foreach (var state in StateSpace)
                {
                    foreach (var state2 in StateSpace)
                    {
                        KeyValuePair<TState,TState> kvp = new KeyValuePair<TState,TState>(state,state2);
                       newParameters[kvp,i] = alphas[state][i]*betas[state2][i+1]*NewTransitionMatrix[state,state2]*NewEmissionMatrix[state2,SequenceOfObservations[i+1]];
                       sum += newParameters[kvp, i + 1];
                    }
                }
                foreach (var state in StateSpace)
                {
                    foreach (var state2 in StateSpace)
                    {
                        KeyValuePair<TState, TState> kvp = new KeyValuePair<TState, TState>(state, state2);
                        newParameters[kvp, i + 1] /= sum;
                    }
                }
            }   
        }

        private double likelihood()
        {
            double l = 0;
            for (var i = 0; i < SequenceOfObservations.Length; i++)
            {
                l += Math.Log(sequenceProbabilities[i]);
            }
            return l;
        }
        public override void DoWork() //based on https://en.wikipedia.org/wiki/Viterbi_algorithm#Pseudocode
        {
            base.DoWork();
            sequenceProbabilities = new double[SequenceOfObservations.Length];
            initDictionaries();
            forward();
            backward();
            gamma();
            recomputeMatrices();
            int iterations = 0;
            double DELTA =0.00001;
            double delta=1, currentLikelyhood, previousLikelyhood, numA, numB, denA, denB;
            previousLikelyhood = likelihood();
            //ugly but... initialization
            do
            {
                iterations++;
                foreach (var state in StateSpace)
                {
                    initialStateProbabilities[state] = gammas[state][0];
                }
                foreach (var state in StateSpace)
                {
                    denA = 0.0;
                    for (var i = 0; i < SequenceOfObservations.Length - 1; i++)
                    {
                        denA += gammas[state][i];
                    }
                    foreach (var state2 in StateSpace)
                    {
                        numA = 0.0;
                        for (var i = 0; i < ObservationSpace.Length; i++)
                        {
                            KeyValuePair<TState, TState> kvp = new KeyValuePair<TState, TState>(state2, state);

                            numA += newParameters[kvp, i];
                        }
                        NewTransitionMatrix[state, state2] = numA / denA;

                    }

                    denB = denA + gammas[state][SequenceOfObservations.Length - 1];
                    for (var k = 0; k < ObservationSpace.Length; k++)
                    {
                        numB = 0.0;
                        for (var i = 0; i < SequenceOfObservations.Length; i++)
                        {
                            //really ugly workaround
                            if (SequenceOfObservations[i].Equals(ObservationSpace[k]))
                                numB += gammas[state][i];
                        }
                        NewEmissionMatrix[state, ObservationSpace[k]] = numB / denB;
                    }

                }


                foreach (var state in StateSpace)
                {
                    var yetAnotherScalingFactor = 0.0;
                    foreach (var state2 in StateSpace)
                    {
                        yetAnotherScalingFactor += NewTransitionMatrix[state, state2];

                    }
                    foreach (var state2 in StateSpace)
                    {
                        NewTransitionMatrix[state, state2] /= yetAnotherScalingFactor;
                    }
                }
                forward();
               backward();
               gamma();
               recomputeMatrices();


                currentLikelyhood = likelihood();
                delta = currentLikelyhood - previousLikelyhood;
                previousLikelyhood = currentLikelyhood;
            }
            while (/*delta > DELTA &&*/ iterations < MAX_ITERATIONS);


            foreach (var state in StateSpace)
            { 
                var yetAnotherScalingFactor = 0.0;
                foreach (var state2 in StateSpace)
                {
                    yetAnotherScalingFactor += NewTransitionMatrix[state, state2];

                }
                foreach (var state2 in StateSpace)
                {
                    NewTransitionMatrix[state, state2] /= yetAnotherScalingFactor;
                }
            }
        }
#else
    private struct ArgMax
    {
        public double Arg;
        public double Max;
    }
        public override void DoWork() //based on https://en.wikipedia.org/wiki/Viterbi_algorithm#Pseudocode
        {
            base.DoWork();
            double[,] T1 = new double[StateSpace.Length, T];
            double[,] T2 = new double[StateSpace.Length, T];

            for (int i = 0; i < StateSpace.Length; i++)
            {
                var state = StateSpace[i];
                T1[i, 0] = InitialProbabilitiesOfStates[i] * EmissionMatrix[i, 0];
                T2[i, 0] = 0;
            }

            for (int i = 1; i < T; i++)
            {
                for (int j = 0; j < StateSpace.Length; j++)
                {
                    var argmax = ArgMax(T1, i, j);

                    T1[j, i] = argmax.max;
                    T2[j, i] = argmax.argmax;
                }
            }
        }

        private dynamic ArgMax(double[,] T1, int i, int j)
        {
            var max = double.MinValue;
            var argmax = double.MinusOne;
            for (int k = 0; k < T1.Length; k++)
            {
                var value = T1[k, i - 1] * TransitionMatrix[k, j] * EmissionMatrix[j, i];
                if (value <= max) continue;
                max = value;
                argmax = k;
            }

            if (argmax == double.MinusOne)
                throw new Exception();

            return new { max, argmax };
        }
#endif
    }
}