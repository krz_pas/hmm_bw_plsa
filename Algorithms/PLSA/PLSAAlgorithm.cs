﻿using HiddenMarkov.Algorithms.PLSA.Model;
using HMMDishonestCasino.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace HiddenMarkov.Algorithms.PLSA
{

    static class ExtensionMethods
    {
        public static double MatrixSumByRow(this MatrixHashTable<Term, Topic, double> value, Term term, List<Topic> topics)
        {
            //
            // Uppercase the first letter in the string.
            //
            /*   if (value.GetLength() > 0)
               {
                   char[] array = value.ToCharArray();
                   array[0] = char.ToUpper(array[0]);
                   return new string(array);
               }*/
            double result = 0.0;
            foreach (var topic in topics)
            {
                result += value[term, topic];
            }
            return result;
        }
    }
    class PLSAAlgorithm
    {
        MatrixHashTable<Term, Document, int> TermsByDocumentMatrix;
        MatrixHashTable<Term, Topic, double> TermsByTopicMatrix;
        MatrixHashTable<Topic, Document, double> TopicByDocumentMatrix;
        List<Document> _documents;
        List<Term> _terms;
        List<Topic> _topics;
        public double convergence { get; set; }
        public int maxIter { get; set; }
        public PLSAAlgorithm()
        {
            maxIter = 1000;
            convergence = 0.00001;
        }
        public PLSAAlgorithm(MatrixHashTable<Term, Document, int> inputData, List<Term> terms, List<Document> documents, List<Topic> topics)
        {
            this.TermsByDocumentMatrix = inputData;
            this._terms = terms;
            this._documents = documents;
            this._topics = topics;
            initData();
        }

        void initData()
        {
            TermsByTopicMatrix = new MatrixHashTable<Term, Topic, double>(_terms, _topics);
            TopicByDocumentMatrix = new MatrixHashTable<Topic, Document, double>(_topics, _documents);
            Random randomGenerator = new Random();
            double normalizingFactor = 0.0;
            foreach (var term in _terms)
            {
                foreach (var topic in _topics)
                {
                    TermsByTopicMatrix[term, topic] = randomGenerator.NextDouble();
                    normalizingFactor += TermsByTopicMatrix[term, topic];
                }
                // var normalizingFactor = TermsByTopicMatrix.Aggregate();


                foreach (var topic in _topics)
                {
                    TermsByTopicMatrix[term, topic] = TermsByTopicMatrix[term, topic] / normalizingFactor;
                }
                normalizingFactor = 0.0;
            }

            foreach (var topic in _topics)
            {
                foreach (var document in _documents)
                {
                    TopicByDocumentMatrix[topic, document] = randomGenerator.NextDouble();
                    normalizingFactor += TopicByDocumentMatrix[topic, document];
                }
                // var normalizingFactor = TermsByTopicMatrix.Aggregate();


                foreach (var document in _documents)
                {
                    TopicByDocumentMatrix[topic, document] = TopicByDocumentMatrix[topic, document] / normalizingFactor;
                }
                normalizingFactor = 0.0;
            }
        }

        public void DoWork()
        {
            for (int i = 0; i < maxIter; i++)
            {
                DoCalculate();
            }
        }

        private void DoCalculateTermsByTopic()
        {
            double tmp = 0.0;
            double enumerator = 0.0;
            double denominator = 0.0;
            double normalizingFactor = 0.0;
            foreach (var topic in _topics)
            {
                normalizingFactor = 0.0;
                foreach (var term in _terms)
                {
                    tmp = 0.0;
                    foreach (var doc in _documents)
                    {
                        denominator = 0.0;
                        enumerator = 0.0;
                        foreach (var top in _topics)
                        {
                            denominator += TermsByTopicMatrix[term, top] * TopicByDocumentMatrix[top, doc];
                        }
                        enumerator = TermsByDocumentMatrix[term, doc] * TopicByDocumentMatrix[topic, doc];
                        tmp += enumerator / denominator;
                    }
                    TermsByTopicMatrix[term, topic] = TermsByTopicMatrix[term, topic] * tmp;
                    normalizingFactor += TermsByTopicMatrix[term, topic];
                    //TermsByTopicMatrix
                }
                //normalization
                foreach (var term in _terms)
                {
                    TermsByTopicMatrix[term, topic] = TermsByTopicMatrix[term, topic] / normalizingFactor;
                }
            }

        }
        private void DoCalculateTopicByDocument()
        {

            double tmp = 0.0;
            double enumerator = 0.0;
            double denominator = 0.0;
            double normalizingFactor = 0.0;
            foreach (var topic in _topics)
            {
                normalizingFactor = 0.0;
                foreach (var doc in _documents)
                {
                    tmp = 0.0;
                    foreach (var term in _terms)
                    {
                        denominator = 0.0;
                        enumerator = 0.0;
                        foreach (var top in _topics)
                        {
                            denominator += TermsByTopicMatrix[term, top] * TopicByDocumentMatrix[top, doc];
                        }
                        enumerator = TermsByDocumentMatrix[term, doc] * TermsByTopicMatrix[term, topic];
                        tmp += enumerator / denominator;
                    }
                    TopicByDocumentMatrix[topic, doc] = TopicByDocumentMatrix[topic, doc] * tmp;
                    normalizingFactor += TopicByDocumentMatrix[topic, doc];
                    //TermsByTopicMatrix
                }
                //normalization
                foreach (var doc in _documents)
                {
                    TopicByDocumentMatrix[topic, doc] = TopicByDocumentMatrix[topic, doc] / normalizingFactor;
                }
            }

        }

        private void DoCalculate()
        {
            DoCalculateTermsByTopic();
            DoCalculateTopicByDocument();
        }

    }
}
