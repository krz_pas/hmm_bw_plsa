﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMarkov.Algorithms.PLSA.Model
{
    class Term
    {
        public String word { get; set; }
        public Term(String str)
        {
            this.word = str;
        }
    }
}
