﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMarkov.Algorithms.PLSA.Model
{
    class Topic
    {
        public String name { get; set; }
        public Topic(String str)
        {
            this.name = str;
        }
    }
}
