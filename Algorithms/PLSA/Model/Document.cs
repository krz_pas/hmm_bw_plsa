﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HiddenMarkov.Algorithms.PLSA.Model
{
    class Document
    {
        public String name { get; set; }
        public Document(String str)
        {
            this.name = str;
        }
    }
}
