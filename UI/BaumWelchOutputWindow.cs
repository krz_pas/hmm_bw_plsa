﻿using HMMDishonestCasino.Collections;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMMDishonestCasino.UI
{
    public partial class BaumWelchOutputWindow : Form
    {
        public BaumWelchOutputWindow(MatrixHashTable<StateSpace, int, double> emissionMatrix, MatrixHashTable<StateSpace, StateSpace, double> transitionMatrix)
        {
            InitializeComponent();



            for (int i = 1; i < 7; i++)
            {
                fairDiceDataGridView.Rows.Add(i,emissionMatrix[StateSpace.FairDice,i]);
                unfairDiceProbalbilitiesOfEachNumberDataGridView.Rows.Add(i, emissionMatrix[StateSpace.LoadedDice, i]);
            }

            switchToFairDiceProbabilityNumericUpDown.Value = (decimal)transitionMatrix[StateSpace.LoadedDice, StateSpace.FairDice];
            switchToUnfairDiceProbabilityNumericUpDown.Value = (decimal)transitionMatrix[StateSpace.FairDice, StateSpace.LoadedDice];
        }

        private void unfairDiceProbalbilitiesOfEachNumberDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
