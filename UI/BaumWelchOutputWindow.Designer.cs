﻿namespace HMMDishonestCasino.UI
{
    partial class BaumWelchOutputWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView = new System.Windows.Forms.DataGridView();
            this.NumbersColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProbabilitiesColumn = new HMMDishonestCasino.Controls.DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.switchToFairDiceProbabilityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fairDiceDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewNumericUpDownColumn1 = new HMMDishonestCasino.Controls.DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn();
            this.switchToUnfairDiceProbabilityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unfairDiceProbalbilitiesOfEachNumberDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchToFairDiceProbabilityNumericUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fairDiceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchToUnfairDiceProbabilityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.unfairDiceProbalbilitiesOfEachNumberDataGridView);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.switchToFairDiceProbabilityNumericUpDown);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(523, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox1.Size = new System.Drawing.Size(479, 434);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Unfair dice properties";
            // 
            // unfairDiceProbalbilitiesOfEachNumberDataGridView
            // 
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.AllowUserToAddRows = false;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.AllowUserToDeleteRows = false;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumbersColumn,
            this.ProbabilitiesColumn});
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.Location = new System.Drawing.Point(10, 88);
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.Name = "unfairDiceProbalbilitiesOfEachNumberDataGridView";
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.ReadOnly = true;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.Size = new System.Drawing.Size(459, 334);
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.TabIndex = 0;
            this.unfairDiceProbalbilitiesOfEachNumberDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.unfairDiceProbalbilitiesOfEachNumberDataGridView_CellContentClick);
            // 
            // NumbersColumn
            // 
            this.NumbersColumn.HeaderText = "Number";
            this.NumbersColumn.MinimumWidth = 100;
            this.NumbersColumn.Name = "NumbersColumn";
            this.NumbersColumn.ReadOnly = true;
            // 
            // ProbabilitiesColumn
            // 
            this.ProbabilitiesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProbabilitiesColumn.DecimalPlaces = 3;
            this.ProbabilitiesColumn.HeaderText = "Probability";
            this.ProbabilitiesColumn.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.ProbabilitiesColumn.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ProbabilitiesColumn.Name = "ProbabilitiesColumn";
            this.ProbabilitiesColumn.ReadOnly = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 69);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Probabilities of each number:";
            // 
            // switchToFairDiceProbabilityNumericUpDown
            // 
            this.switchToFairDiceProbabilityNumericUpDown.DecimalPlaces = 3;
            this.switchToFairDiceProbabilityNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.switchToFairDiceProbabilityNumericUpDown.Location = new System.Drawing.Point(170, 38);
            this.switchToFairDiceProbabilityNumericUpDown.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.switchToFairDiceProbabilityNumericUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.switchToFairDiceProbabilityNumericUpDown.Name = "switchToFairDiceProbabilityNumericUpDown";
            this.switchToFairDiceProbabilityNumericUpDown.ReadOnly = true;
            this.switchToFairDiceProbabilityNumericUpDown.Size = new System.Drawing.Size(115, 20);
            this.switchToFairDiceProbabilityNumericUpDown.TabIndex = 11;
            this.switchToFairDiceProbabilityNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 40);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Switch to fair dice probability =";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.fairDiceDataGridView);
            this.groupBox2.Controls.Add(this.switchToUnfairDiceProbabilityNumericUpDown);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox2.Size = new System.Drawing.Size(523, 434);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Fair dice properties";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Probabilities of each number:";
            // 
            // fairDiceDataGridView
            // 
            this.fairDiceDataGridView.AllowUserToAddRows = false;
            this.fairDiceDataGridView.AllowUserToDeleteRows = false;
            this.fairDiceDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.fairDiceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fairDiceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewNumericUpDownColumn1});
            this.fairDiceDataGridView.Location = new System.Drawing.Point(14, 88);
            this.fairDiceDataGridView.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.fairDiceDataGridView.Name = "fairDiceDataGridView";
            this.fairDiceDataGridView.ReadOnly = true;
            this.fairDiceDataGridView.Size = new System.Drawing.Size(509, 334);
            this.fairDiceDataGridView.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Number";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewNumericUpDownColumn1
            // 
            this.dataGridViewNumericUpDownColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewNumericUpDownColumn1.DecimalPlaces = 3;
            this.dataGridViewNumericUpDownColumn1.HeaderText = "Probability";
            this.dataGridViewNumericUpDownColumn1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.dataGridViewNumericUpDownColumn1.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dataGridViewNumericUpDownColumn1.Name = "dataGridViewNumericUpDownColumn1";
            this.dataGridViewNumericUpDownColumn1.ReadOnly = true;
            // 
            // switchToUnfairDiceProbabilityNumericUpDown
            // 
            this.switchToUnfairDiceProbabilityNumericUpDown.DecimalPlaces = 3;
            this.switchToUnfairDiceProbabilityNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.switchToUnfairDiceProbabilityNumericUpDown.Location = new System.Drawing.Point(186, 38);
            this.switchToUnfairDiceProbabilityNumericUpDown.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.switchToUnfairDiceProbabilityNumericUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.switchToUnfairDiceProbabilityNumericUpDown.Name = "switchToUnfairDiceProbabilityNumericUpDown";
            this.switchToUnfairDiceProbabilityNumericUpDown.ReadOnly = true;
            this.switchToUnfairDiceProbabilityNumericUpDown.Size = new System.Drawing.Size(115, 20);
            this.switchToUnfairDiceProbabilityNumericUpDown.TabIndex = 9;
            this.switchToUnfairDiceProbabilityNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 40);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Switch to unfair dice probability =";
            // 
            // BaumWelchOutputWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 434);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "BaumWelchOutputWindow";
            this.Text = "BaumWelchOutputWindow";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unfairDiceProbalbilitiesOfEachNumberDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchToFairDiceProbabilityNumericUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fairDiceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.switchToUnfairDiceProbabilityNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView unfairDiceProbalbilitiesOfEachNumberDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumbersColumn;
        private Controls.DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn ProbabilitiesColumn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView fairDiceDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private Controls.DataGridViewNumericUpDownElements.DataGridViewNumericUpDownColumn dataGridViewNumericUpDownColumn1;
        private System.Windows.Forms.NumericUpDown switchToFairDiceProbabilityNumericUpDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown switchToUnfairDiceProbabilityNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;

    }
}